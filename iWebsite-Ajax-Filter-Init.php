<?php

class iWebsite_Ajax_Woo_Filter {

	public function __construct() {

		add_action( 'woocommerce_before_shop_loop', array ( $this, 'iwebsite_ajax_filter_trigger_bar' ), 20 );
		add_action( 'woocommerce_before_shop_loop', array ( $this, 'iwebsite_ajax_filter_attributes_containers' ), 50 );
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
		add_action( 'wp_ajax_iwebsite_ajax_filter_action', array ( $this, 'iwebsite_ajax_filter_action' ) );
		add_action( 'wp_ajax_nopriv_iwebsite_ajax_filter_action', array ( $this, 'iwebsite_ajax_filter_action' ) );
		add_action( 'woocommerce_before_shop_loop', array ( $this, 'open_bar_container' ), 1 );

	}

	/**
	 * @return string
	 */

	public function open_bar_container() {
		echo '<div class="filter-container">';
	}

	public function iwebsite_ajax_filter_action() {
		// ob_start();
		$args       = array (
			'posts_per_page' => - 1,
			'post_type'      => array ( 'product' ),
			'post_status'    => 'publish'
		);
		if ( isset( $_POST[ 'iwebsite_filter_attr' ] ) && ! empty( $_POST[ 'iwebsite_filter_attr' ] ) ) {
			$attr_array = $_POST[ 'iwebsite_filter_attr' ];
			// show( $attr_array, 'attr_array attr_array attr_array' );

			$tax_count  = 0;
			if ( isset( $attr_array[ 'color' ] ) && ! empty( $attr_array[ 'color' ] ) ) {
				$args[ 'tax_query' ][ 'relation' ] = 'AND';
				foreach ( $attr_array[ 'color' ] as $color_id ) {
					$args[ 'tax_query' ][ $tax_count ][ 'relation' ] = 'OR';
					$args[ 'tax_query' ][ $tax_count ][]             = array ( "taxonomy" => 'pa_color', "terms" => $color_id );
				}
				$tax_count ++;
			}
			if ( isset( $attr_array[ 'size' ] ) && ! empty( $attr_array[ 'size' ] ) ) {
				$args[ 'tax_query' ][ 'relation' ] = 'AND';
				foreach ( $attr_array[ 'size' ] as $size_id ) {
					$args[ 'tax_query' ][ $tax_count ][ 'relation' ] = 'OR';
					$args[ 'tax_query' ][ $tax_count ][]             = array ( "taxonomy" => 'pa_size', "terms" => $size_id );
				}
				$tax_count ++;
			}
			if ( isset( $attr_array[ 'category' ] ) && ! empty( $attr_array[ 'category' ] ) ) {
				$args[ 'tax_query' ][ 'relation' ] = 'AND';
				foreach ( $attr_array[ 'category' ] as $term_id ) {
					$args[ 'tax_query' ][ $tax_count ][ 'relation' ] = 'AND';
					$args[ 'tax_query' ][ $tax_count ][]             = array ( "taxonomy" => 'product_cat', "terms" => $term_id );
				}
				$tax_count ++;
			}
			$args[ 'meta_query' ][ 'relation' ] = 'AND';
			
			$args[ 'meta_query' ][]             = array (
				'key'     => '_price',
				'value'   => array ( $attr_array[ 'price' ][ 0 ], $attr_array[ 'price' ][ 1 ] ),
				'compare' => 'BETWEEN',
				'type'    => 'NUMERIC'
			);
			
			$args['meta_query'][] = array(
				'key' 	=> '_stock_status',
				'value' => 'instock',
			);
		}

		ob_start();
		$wp_query = new WP_Query( $args );
		$qw_num   = $wp_query->post_count;
		if ( $wp_query->have_posts() ) {
 			$ar_selected_colors 	= array();
	       	$ar_selected_sizes 		= array();
	       	if ( isset( $args['tax_query'] ) && !empty( $args['tax_query'] ) ){
			  	unset( $args['tax_query']['relation'] );
		       	$last_element_index_in_query_ar = count( $args['tax_query'] ) - 1;
		       	$last_element_in_query 			= $args['tax_query'][$last_element_index_in_query_ar];
		       	if( isset( $last_element_in_query['taxonomy'] ) && $last_element_in_query['taxonomy'] == 'product_cat' ) {
		       		unset( $args['tax_query'][$last_element_index_in_query_ar] );
		       	}
	      		$tax_query 				= $args['tax_query'];
		      
				foreach ( $tax_query as $key => $ar_tax ) {
					unset( $tax_query[$key]['relation'] );
				}

				// we save to arrays colors and sizes that have been chosen by user
		       	foreach ( $tax_query as $key => $ar_tax ) {
					foreach ( $ar_tax as $ar_tax_key => $tax ){
		       			if ( $tax['taxonomy'] == 'pa_color' ){
							$ar_selected_colors[] = $tax['terms'];
		       			} elseif ( $tax['taxonomy'] == 'pa_size' ){
		       				$ar_selected_sizes[] = $tax['terms'];
		       			}
		       		}
		       	}
	       	}
			while ( $wp_query->have_posts() ) {
				$wp_query->the_post();
				$flag_display 	= true;	
				$product_id 	= get_the_id();
				if ( !$product_id ){
					continue;
				}
				$product 	= new WC_Product_Variable( $product_id );
				if ( !$product || $product == NULL || empty( $product ) ){
					continue;
				}
				// show( $product, 'product' );
				echo '<div style="max-width:200px;">';
				show( $product->get_manage_stock(), 'get  get_manage_stock' );
				echo '</div>';
				$product_variations 	= $product->get_available_variations();
				if ( $product_variations && is_array( $product_variations ) ){
					// get all product variation
					foreach ( $product_variations as $variation_key => $variation ) {
						$atr_size 			= isset( $variation['attributes']['attribute_pa_size'] )? $variation['attributes']['attribute_pa_size'] : false ;
						$atr_color 			= isset( $variation['attributes']['attribute_pa_color'] )? $variation['attributes']['attribute_pa_color'] : false;
						if ( $atr_size ){
							$term_size		= get_term_by( 'slug', $atr_size, 'pa_size' );
							$term_size_id	= $term_size->term_id;
						} else {
							$term_size_id 	= 'not value';
						}
						if ( $atr_color ){
							$term_color		= get_term_by( 'slug', $atr_color, 'pa_color' );
							$term_color_id	= $term_color->term_id;
						} else {
							$term_color_id 	= 'not value';
						}
						// we are checking if variation has chosen color or size we get in
						if ( !empty( $ar_selected_sizes ) && !empty( $ar_selected_colors ) ){
							if ( in_array( $term_size_id,  $ar_selected_sizes ) && in_array( $term_color_id,  $ar_selected_colors ) ){
							
								// important if variation out of stock then do not displaying it, but if in stock we displaying it and break the cycle 
								if ( !$variation['is_in_stock'] ){
									$flag_display = false;
								} else {
									$flag_display = true;	
									break;
								}	
							} 
						} else {
							if ( ( !empty( $ar_selected_sizes ) && in_array( $term_size_id,  $ar_selected_sizes ) ) || ( !empty( $ar_selected_colors ) && in_array( $term_color_id,  $ar_selected_colors ) ) ){
							
								// important if variation out of stock then do not displaying it, but if in stock we displaying it and break the cycle 
								if ( !$variation['is_in_stock'] ){
									$flag_display = false;
								} else {
									$flag_display = true;	
									break;
								}	
							} 
						}
					}
					// show( $flag_display, 'flag_display' );
					// if variation in stock( we did break in cycle ) - dysplaying it
				}
				if ( $flag_display ){
					wc_get_template_part( 'content', 'product' );
				} 
			}
		} else {
			wc_get_template( 'loop/no-products-found.php' );
		}

		$data = ob_get_clean();
		wp_send_json_success( array ( 'data' => $data, 'result_num' => $qw_num ) );
	}

	public function iwebsite_ajax_filter_trigger_bar() {
		$count = wc_get_loop_prop( 'total' );
		?>
		<div class="iwebsite_ajax_filter_active_attr_bar">
		</div>
		<div class="iwebsite-ajax-filter-trigger-bar">
			<ul class="filter-list">
				<!-- <li><span><?php echo __( 'Filter by:', 'iwebsite-ajax-filter' ); ?></span></li> -->
				<!-- <li><a data-filter-trigger="category"><i class="pe-7s-note2"></i><?php echo __( 'Categories', 'iwebsite-ajax-filter' ); ?></a></li> -->
				<li><a data-filter-trigger="color"><?php echo __( 'Colors', 'iwebsite-ajax-filter' ); ?><i class="pe-7s-angle-down"></i></a></li>
				<li><a data-filter-trigger="size"><?php echo __( 'Size', 'iwebsite-ajax-filter' ); ?><i class="pe-7s-angle-down"></i></a></li>
				<li><a data-filter-trigger="price"><?php echo __( 'Price', 'iwebsite-ajax-filter' ); ?><i class="pe-7s-angle-down"></i></a></li>
				<li><span class="iwebsite-ajax-filter-results-count"><?php echo $count . ' ' . __( 'results', 'iwebsite-ajax-filter' ); ?></span></li>
			</ul>
		</div>
		<?php

	}

	public function iwebsite_ajax_filter_attributes_containers() {
		$categories = get_terms( array (
			'taxonomy'   => 'product_cat',
			'hide_empty' => true,
		) );
		$color_attr = get_terms( array (
			'taxonomy'   => 'pa_color',
			'hide_empty' => true,
		) );
		$size_attr  = get_terms( array (
			'taxonomy'   => 'pa_size',
			'hide_empty' => true,
		) );
		?>
		<div class="wide-attribute-containers">
			<div class="color-attribute-filter-container filter-container-atribute">
				<ul>
					<?php foreach ( $color_attr as $color ) {
						if ( class_exists( 'iwebsite_Attribute_Image' ) ){
							$iwebsite_attribute = new iwebsite_Attribute_Image( 'thumbnail_id', $color->term_id, 'pa_color' );
							$image_attr         = $iwebsite_attribute->iwebsite_image_src();
						} else {
							$image_attr 		= '';
						}
						?>
						<li>
							<a href="" class="color-attribute-filter-item color-trigger-attr-<?php echo $color->term_id; ?>" data-taxonomy-name="pa_color" data-attribute-id="<?php echo $color->term_id; ?>" data-label="<?php echo $color->name; ?>">
								<?php if ( $image_attr != '' ){ ?><span class="status-attr" style="background-image: url('<?php echo $image_attr; ?>')"></span><?php } ?>
								<span class="attr-label"><?php echo $color->name; ?></span>
							</a>
						</li>
						<?php

					} ?>
				</ul>
			</div>
			<div class="size-attribute-filter-container filter-container-atribute">
				<ul>
					<?php foreach ( $size_attr as $size ) {
						?>
						<li><a href="" class="size-attribute-filter-item  size-trigger-attr-<?php echo $size->term_id; ?>" data-taxonomy-name="pa_size" data-attribute-id="<?php echo $size->term_id; ?>" data-label="<?php echo $size->name; ?>">
								<span class="status-attr size-status-attr">	</span>
								<span class="attr-label"><?php echo $size->name; ?></span>
							</a>
						</li>
						<?php

					} ?>
				</ul>
			</div>

			<div class="price-attribute-filter-container filter-container-atribute">
				<?php

				global $wpdb;
				$results = $wpdb->get_col( "
				SELECT pm.meta_value
				FROM {$wpdb->prefix}term_relationships as tr
				INNER JOIN {$wpdb->prefix}term_taxonomy as tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
				INNER JOIN {$wpdb->prefix}terms as t ON tr.term_taxonomy_id = t.term_id
				INNER JOIN {$wpdb->prefix}postmeta as pm ON tr.object_id = pm.post_id
				WHERE tt.taxonomy LIKE 'product_cat'
				AND pm.meta_key = '_price'
			" );

				// Sorting prices numerically
				sort( $results, SORT_NUMERIC );
				// Get the min and max prices
				$min = current( $results );
				$max = end( $results );
				?>
				<div class="price-slider-iwebsite-ajax-filter" data-min-price="<?php echo $min; ?>" data-max-price="<?php echo $max; ?>">

				</div>
				<span><?php echo __( 'Price:', 'iwebsite-ajax-filter' ); ?></span>
				<span class="price-slider-iwebsite-ajax-filter-value"></span>
			</div>
		</div>
		</div>
		<?php /*
		<div class="sidebar-categories-container-iwebsite-filter" style="display:none">
			<?php
			echo $this->category_recursive_check( $categories, 0 );
			?>
		</div>
		*/ ?>
		<?php

	}


	public function category_recursive_check( $array, $parent ) {
		if ( ! is_array( $array ) OR empty( $array ) ) {
			return false;
		}

		if ( $parent == 0 ) {
			$output = '<ul class="cat-filter-iwebsite" >';
		} else {
			$output = '<ul class="sub-item" >';

		}
		foreach ( $array as $key => $value ){
			$children = get_terms( 'product_cat', array (
				'parent'     => $value->term_id,
				'hide_empty' => true
			) );
			if ( $value->parent == $parent ) {
				if ( ! empty( $children ) ) {
					$output .= '<li class="filter-parent-item">';
					$output .= '<a href="javascript:void(0);" class="accordion">
									<span class="icon pe-7s-plus"></span>
								</a>';
				} else {
					$output .= '<li>';
				}
				if ( $value->parent == 0 || ! empty( $children )) {
					$output .= '<a href="" class="category-filter-item filter-child-item" data-attribute-id="' . $value->term_id . '">' . $value->name . '</a>';
					$output .= $this->category_recursive_check( $children, $value->term_id );
				} else {
					$output .= '<a href="" class="category-filter-item filter-child-item" data-attribute-id="' . $value->term_id . '">' . $value->name . '</a>';
					$output .= '</li>';
				}
			}

		}

		$output .= '</ul>';

		return $output;
	}
}
$object = new iWebsite_Ajax_Woo_Filter();

// debug info

// add_action( 'woocommerce_after_shop_loop_item_title', 'display_product_variations_for_debug' );

function display_product_variations_for_debug(){
	global $product;
	$user = wp_get_current_user();
	echo '<div class="only-admin" style="direction:ltr!important;max-width:300px">';
	$product_id = $product->get_id();
	$product = new WC_Product_Variable( $product_id );
	$postmeta = get_post_meta( $product_id );
	$product_variations = $product->get_available_variations();
	foreach ( $product_variations as $variation_key => $variation ) {
		$size_attr = $variation['attributes']['attribute_pa_size'];
		$color_attr = $variation['attributes']['attribute_pa_color'];
		// $stock = trim( $variation['availability_html'] );
		$stock = $variation['is_in_stock'] != ''? $variation['is_in_stock'] : 0;
		$variation_id = $variation['variation_id'];
		$variation_is_active = $variation['variation_is_active'];

		// show( $variation_is_active, 'variation_is_active' );
		// show( $variation_id, 'variation_id' );


		echo ' <div>SIZE: '. $size_attr. '</div>'; 
		echo '<div>STOCK: '. $stock .'</div>';
		echo '<div>COLOR: '. $color_attr .'</div>';
		echo '<div>------------------</div>';
	}
	show( $postmeta, 'postmeta' );
	echo '</div>';
}
