<?php
/**
 * Plugin Name: iWebsite Ajax Filter
 * Version: 1.0.0
 * Plugin URI: https://iwebsite.co.il
 * Description: Plugin for filtering products by ajax
 * Author: iWebsite
 * Author URI: https://iwebsite.co.il
 * Text Domain: iwebsite-ajax-filter
 * Domain Path: /languages
 */

if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

if ( ! defined( 'dirname(__FILE__)' ) ) {
	define( 'AJAX_FILTER_IWEBSITE_VERSION', '1.0.1' );
	define( 'AJAX_FILTER_IWEBSITE_PLUGIN_PATH', dirname( __FILE__ ) );
	define( 'AJAX_FILTER_IWEBSITE_PLUGIN_PATH_DIR', plugin_dir_url( __FILE__ ) );
	define( 'AJAX_FILTER_IWEBSITE_PLUGIN_FOLDER', basename( dirname( __FILE__ ) ) );

}

if ( ! class_exists( 'iWebsite_Ajax_Filter_Plugin' ) ) {

	class iWebsite_Ajax_Filter_Plugin {

		public function __construct() {
			add_action( 'plugins_loaded', array ( $this, 'iwebsite_ajax_filter_activated' ) );
			add_action( 'wp_enqueue_scripts', array ( $this, 'iwebsite_ajax_filter_enqueue_style' ), 105 );

			add_action('plugins_loaded',  array ( $this, 'localize_iwebsite_ajax_filter' ) );


		}
		public function localize_iwebsite_ajax_filter() {
			load_plugin_textdomain('iwebsite-ajax-filter', false, dirname( plugin_basename(__FILE__) ) . '/languages/');
		}
		public function iwebsite_ajax_filter_enqueue_style() {

			wp_enqueue_script( 'jquery-ui-slider' );
			wp_localize_script( 'jquery', 'iwebsiteAjaxUrl', admin_url( 'admin-ajax.php' ) );
			wp_enqueue_script( 'ajax-filter-js', AJAX_FILTER_IWEBSITE_PLUGIN_PATH_DIR . '/js/script.js', array ( 'jquery' ), '', true );
			wp_enqueue_style( 'ajax-filter-css', AJAX_FILTER_IWEBSITE_PLUGIN_PATH_DIR. '/css/style.css', '', '' );
			$wp_scripts = wp_scripts();
			wp_enqueue_style( 'jquery-ui-theme-smoothness', sprintf( '//ajax.googleapis.com/ajax/libs/jqueryui/%s/themes/smoothness/jquery-ui.css', // working for https as well now
				$wp_scripts->registered[ 'jquery-ui-core' ]->ver ) );
			}

		public function iwebsite_ajax_filter_activated() {
			require_once( AJAX_FILTER_IWEBSITE_PLUGIN_PATH . '/iWebsite-Ajax-Filter-Init.php' );
		}

	}
	$iWebsite = new iWebsite_Ajax_Filter_Plugin();
}



