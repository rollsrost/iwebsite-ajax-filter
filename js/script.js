jQuery(function ($) {
    $('.iwebsite-ajax-filter-trigger-bar .filter-list>li').on('click', function () {
        let triger = $(this).find('a').attr('data-filter-trigger');
        if ($(this).hasClass('active')) {


            if (triger != 'category') {
                $('.iwebsite-ajax-filter-trigger-bar .filter-list>li ').each(function () {
                    $(this).removeClass('active');
                });

                $('.filter-container-atribute').each(function () {
                    $(this).removeClass('active');
                });
                $('.' + triger + '-attribute-filter-container').removeClass('active');
            } else {
                $(this).removeClass('active');

            }


        } else {
            if (triger != 'category') {

                $('.iwebsite-ajax-filter-trigger-bar .filter-list>li ').each(function () {
                    $(this).removeClass('active');
                });
                $(this).addClass('active');
                $('.filter-container-atribute').each(function () {
                    $(this).removeClass('active');
                });
                $('.' + triger + '-attribute-filter-container').addClass('active');

            } else {
                $(this).addClass('active');
            }
        }
        if (triger == 'category') {
            let winW = $(window).width();
            if (winW < 850) {
                $(".black-window").fadeIn(200);

            }
            $('.sidebar-categories-container-iwebsite-filter').toggleClass('active');
            $('ul.products').toggleClass('categoties-active');
        }


    });

    var min_price = parseInt($('.price-slider-iwebsite-ajax-filter').attr('data-min-price'));
    var max_price = parseInt($('.price-slider-iwebsite-ajax-filter').attr('data-max-price'));
    var iwebsite_args_complete = {};
    iwebsite_args_complete.price = null;
    iwebsite_args_complete.color = null;
    iwebsite_args_complete.size = null;
    iwebsite_args_complete.category = null;



    iwebsite_args_complete.price = [min_price, max_price];
    $('.price-slider-iwebsite-ajax-filter').slider({
        range: true,
        min: min_price,
        max: max_price,

        values: [min_price, max_price],
        create: function (event, ui) {

            $('.price-slider-iwebsite-ajax-filter span:nth-child(2)').text(min_price);
            $('.price-slider-iwebsite-ajax-filter .ui-slider-handle:last-child').text(max_price);
        },
        slide: function (event, ui) {
            $('.price-slider-iwebsite-ajax-filter span:nth-child(2)').text(ui.values[0]);
            $('.price-slider-iwebsite-ajax-filter .ui-slider-handle:last-child').text(ui.values[1]);
            $('.price-slider-iwebsite-ajax-filter-value').text("₪" + ui.values[0] + " - ₪" + ui.values[1]);

        },

        change: function (event, ui) {
            var min = ui.values[0];
            var max = ui.values[1];
            var price_array = [min, max]
            ajax_call('price', price_array);

        }

    });
    $(".price-slider-iwebsite-ajax-filter-value").text("₪" + $(".price-slider-iwebsite-ajax-filter").slider("values", 0) +
        " - ₪" + $(".price-slider-iwebsite-ajax-filter").slider("values", 1));

    $('.color-attribute-filter-container a').on('click', function (e) {

        e.preventDefault();
        let term_id = $(this).attr('data-attribute-id');
        if ($(this).hasClass('checked')) {
            $('.iwebsite_ajax_filter_active_attr_bar a.color-' + term_id).remove();

        } else {
            $('.iwebsite_ajax_filter_active_attr_bar').append('<a class="color-' + term_id + '" data-term="color" data-term_id="' + term_id + '">' + $(this).attr("data-label") + '</a>')
        }
        $(this).toggleClass('checked');
        var color_array = [];
        $('.color-attribute-filter-container a.checked').each(function () {
            let term_id_checked =  $(this).attr('data-attribute-id')
            color_array.push(term_id_checked);
        });

        if (color_array != []) {
            ajax_call('color', color_array);
        } else {
            ajax_call('color', null);
        }

    });
    $('.size-attribute-filter-container a').on('click', function (e) {
        e.preventDefault();
        let term_id = $(this).attr('data-attribute-id');
        if ($(this).hasClass('checked')) {
            $('.iwebsite_ajax_filter_active_attr_bar a.size-' + term_id).remove();

        } else {
            $('.iwebsite_ajax_filter_active_attr_bar').append('<a class="size-' + term_id + '" data-term="size" data-term_id="' + term_id + '">' + $(this).attr("data-label") + '</a>')
        }
        $(this).toggleClass('checked');
        var size_array = [];
        $('.size-attribute-filter-container a.checked').each(function () {
            size_array.push($(this).attr('data-attribute-id'));
        })
        if (size_array != []) {
            ajax_call('size', size_array);
        } else {
            ajax_call('size', null);
        }

    });
    $('.sidebar-categories-container-iwebsite-filter a.filter-child-item').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('checked');
        var category_array = [];
        $('.sidebar-categories-container-iwebsite-filter a.filter-child-item.checked').each(function () {
            category_array.push($(this).attr('data-attribute-id'));
        });
        if (category_array != []) {
            ajax_call('category', category_array);
        } else {
            ajax_call('category', null);
        }

    });

    $(document).on('click', '.sidebar-categories-container-iwebsite-filter a.accordion', function (e) {
        let toggle_button_i = $(this).children('.icon');
        if (toggle_button_i.hasClass('pe-7s-plus')) {
            toggle_button_i.removeClass('pe-7s-plus');
            toggle_button_i.addClass('pe-7s-less');
        } else {
            toggle_button_i.removeClass('pe-7s-less');
            toggle_button_i.addClass('pe-7s-plus');
        }
        $(this).closest('.filter-parent-item').children('.sub-item').toggleClass('active');

    });
    $(document).on('click', '.iwebsite_ajax_filter_active_attr_bar a', function (e) {

        let term = $(this).attr('data-term');
        let term_id = $(this).attr('data-term_id');
        // console.log( term, 'term' );
        // console.log( term_id, 'term_id' );
        if (term == 'color') {
            let index = iwebsite_args_complete.color.indexOf(term_id);
            // console.log( iwebsite_args_complete, 'iwebsite_args_complete' );            
            // console.log( index, 'index' );
            if (index > -1) {
                iwebsite_args_complete.color.splice(index, 1);
            }
             console.log( iwebsite_args_complete, 'iwebsite_args_complete after splice' );
            $('.color-trigger-attr-' + term_id).removeClass('checked');
            var color_array = [];
            $('.color-attribute-filter-container a.checked').each(function () {
                color_array.push(term_id);
            });
             console.log( color_array, 'color_array pass to ajax' );

            if (color_array != []) {
                ajax_call('color', color_array);
            } else {
                ajax_call('color', null);
            }
        } else if (term == 'size') {
            let index = iwebsite_args_complete.size.indexOf(term_id);
            if (index > -1) {
                iwebsite_args_complete.size.splice(index, 1);
            }
            $('.size-trigger-attr-' + term_id).removeClass('checked');
            var size_array = [];
            $('.size-attribute-filter-container a.checked').each(function () {
                size_array.push($(this).attr('data-attribute-id'));
            })
            if (size_array != []) {
                ajax_call('size', size_array);
            } else {
                ajax_call('size', null);
            }
        }
        $(this).remove();

    });
    function ajax_call(change_field, val) {
        $('ul.products').addClass('ajax-loader');
        iwebsite_args_complete[change_field] = val;
        // console.log( iwebsite_args_complete[change_field], 'POST TO AJAX: field changed' )
        let ajaxSend = {
            action: 'iwebsite_ajax_filter_action',
            iwebsite_filter_attr: iwebsite_args_complete,
        };
        // console.log( ajaxSend.iwebsite_args_complete, 'ajaxSend SDSDSD' )
        // console.log( ajaxSend, 'ajaxSend --- !!! ' )

        jQuery.ajax({
            url: ajaxurl,
            data: ajaxSend,
            type: 'POST',
            async: true,
            success: function (ajaxResponse) {
                console.log( ajaxResponse.data, 'ajaxResponse data' );
                // $('.ajax-filter-products-container').remove();
                $('.products').html('');
                $('.products').append(ajaxResponse.data.data);
                $('.iwebsite-ajax-filter-results-count').empty();
                $('.iwebsite-ajax-filter-results-count').append(ajaxResponse.data.result_num + ' results');
                // if ( $('.products').children().length > 0 ){
                    $('.category-page ul.products').infinitescroll('destroy');
                // }
                $('ul.products').removeClass('ajax-loader');
            },
            error: function (error) {
                $('ul.products').removeClass('ajax-loader');
                console.log( error, 'error' );
            }

        });
    }

});
